import * as vscode from 'vscode';
import * as featureFlags from '../feature_flags';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from './constants';
import { GitLabPlatformManager } from '../platform/gitlab_platform';
import { GitLabPlatformManagerForCodeSuggestions } from './gitlab_platform_manager_for_code_suggestions';
import { DISABLED_BY_PROJECT, ProjectDisabledPolicy } from './state_policy/project_disabled_policy';
import { diffEmitter } from './diff_emitter';
import { StatePolicy } from './state_policy/state_policy';
import {
  DISABLED_VIA_SETTINGS,
  DisabledInSettingsPolicy,
} from './state_policy/disabled_in_settings_policy';
import {
  DISABLED_BY_USER,
  disabledForSessionPolicy,
} from './state_policy/disabled_for_session_policy';
import { MissingAccountPolicy, NO_ACCOUNT } from './state_policy/missing_account_policy';
import {
  MinimumGitLabVersionPolicy,
  UNSUPPORTED_GITLAB_VERSION,
} from './state_policy/minimal_gitlab_version_policy';
import { CombinedPolicy } from './state_policy/combined_policy';
import { LicenseStatusPolicy, NO_LICENSE } from './state_policy/license_status_policy';

type ValueOf<T> = T[keyof T];

export type VisibleCodeSuggestionsState = ValueOf<typeof VisibleCodeSuggestionsState>;
export const VisibleCodeSuggestionsState = {
  DISABLED_VIA_SETTINGS,
  DISABLED_BY_USER,
  NO_ACCOUNT,
  NO_LICENSE,
  READY: 'code-suggestions-global-ready',
  UNSUPPORTED_LANGUAGE: 'code-suggestions-document-unsupported-language',
  DISABLED_BY_PROJECT,
  ERROR: 'code-suggestions-error',
  LOADING: 'code-suggestions-loading',
  UNSUPPORTED_GITLAB_VERSION,
} as const;

const isSupportedLanguage = (editor: vscode.TextEditor) =>
  AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES.includes(editor.document.languageId);

export class CodeSuggestionsStateManager {
  #policies: StatePolicy[] = [];

  // //////////
  // boolean flags that indicate code suggestions are not on

  #isUnsupportedLanguage = false;

  // //////////
  // boolean flags and counters indicating temporary states
  #isInErrorState = false;

  #loadingResources = new WeakSet();

  // this can't be a boolean flag because it's possible that response from first
  // request comes after we send second request (which would incorrectly set loading to false)
  #loadingCounter = 0;
  // //////////

  #subscriptions: vscode.Disposable[] = [];

  #gitlabPlatformManager: GitLabPlatformManager;

  #changeVisibleStateEmitter = diffEmitter(new vscode.EventEmitter<VisibleCodeSuggestionsState>());

  onDidChangeVisibleState = this.#changeVisibleStateEmitter.event;

  onDidChangeDisabledByUserState: vscode.Event<boolean>;

  #manager: GitLabPlatformManagerForCodeSuggestions;

  #userDisabledPolicy: StatePolicy;

  constructor(gitlabPlatformManager: GitLabPlatformManager, context: vscode.ExtensionContext) {
    this.#gitlabPlatformManager = gitlabPlatformManager;
    this.#manager = new GitLabPlatformManagerForCodeSuggestions(this.#gitlabPlatformManager);
    const disabledInSettingsPolicy = new DisabledInSettingsPolicy();
    this.#userDisabledPolicy = new CombinedPolicy(
      disabledForSessionPolicy,
      disabledInSettingsPolicy,
    );
    this.onDidChangeDisabledByUserState = this.#userDisabledPolicy.onEngagedChange;
    const missingAccountPolicy = new MissingAccountPolicy(this.#manager);
    const minimumGitLabVersionPolicy = new MinimumGitLabVersionPolicy(
      this.#manager,
      context,
      new CombinedPolicy(this.#userDisabledPolicy, missingAccountPolicy),
    );

    this.#policies.push(
      disabledInSettingsPolicy,
      disabledForSessionPolicy,
      missingAccountPolicy,
      minimumGitLabVersionPolicy,
    );
    if (featureFlags.isEnabled(featureFlags.FeatureFlag.CodeSuggestionsLicensePolicy)) {
      this.#policies.push(
        new LicenseStatusPolicy(this.#manager, [
          this.#userDisabledPolicy,
          minimumGitLabVersionPolicy,
        ]),
      );
    }
    this.#policies.push(new ProjectDisabledPolicy(this.#manager));
    this.#subscriptions.push(this.#manager);
  }

  async init() {
    await Promise.all(this.#policies.filter(p => !!p.init).map(p => p.init?.()));

    const checkIfLanguageSupported = (te?: vscode.TextEditor) => {
      this.#isUnsupportedLanguage = te ? !isSupportedLanguage(te) : false;
      this.#fireChange();
    };
    checkIfLanguageSupported(vscode.window.activeTextEditor);

    this.#subscriptions.push(
      ...this.#policies.map(p => p.onEngagedChange(() => this.#fireChange())),
    );
    this.#subscriptions.push(
      vscode.window.onDidChangeActiveTextEditor(async te => {
        checkIfLanguageSupported(te);
        this.#fireChange();
      }),
    );
  }

  async getPlatform() {
    return this.#manager.getGitLabPlatform();
  }

  isDisabledByUser() {
    return this.#userDisabledPolicy.engaged;
  }

  /** isActive indicates whether the suggestions are on and suggestion requests are being sent to the API */
  isActive() {
    const noPoliciesEngaged = this.#policies.every(p => !p.engaged);
    return noPoliciesEngaged && !this.#isUnsupportedLanguage;
  }

  getVisibleState(): VisibleCodeSuggestionsState {
    const activePolicy = this.#policies.find(p => p.engaged);
    if (activePolicy) {
      return activePolicy.state!;
    }

    if (this.#isUnsupportedLanguage) {
      return VisibleCodeSuggestionsState.UNSUPPORTED_LANGUAGE;
    }

    if (this.#isInErrorState) {
      return VisibleCodeSuggestionsState.ERROR;
    }

    if (this.#loadingCounter !== 0) {
      return VisibleCodeSuggestionsState.LOADING;
    }

    return VisibleCodeSuggestionsState.READY;
  }

  setLoadingResource(resource: object, isLoading: boolean) {
    const isResourceLoading = this.#loadingResources.has(resource);

    if (isResourceLoading !== isLoading) {
      this.setLoading(isLoading);
    }

    if (isLoading) {
      this.#loadingResources.add(resource);
    } else {
      this.#loadingResources.delete(resource);
    }
  }

  #fireChange = () => {
    this.#changeVisibleStateEmitter.fire(this.getVisibleState());
  };

  setError = (isError: boolean) => {
    this.#isInErrorState = isError;
    this.#fireChange();
  };

  setLoading = (isLoading: boolean) => {
    if (isLoading) {
      this.#loadingCounter += 1;
    } else {
      this.#loadingCounter = Math.max(0, this.#loadingCounter - 1);
    }
    this.#fireChange();
  };

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
